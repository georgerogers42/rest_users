from os import environ
from contextlib import contextmanager
from sqlalchemy import create_engine, Column, Integer, String, Text, text
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from passlib.hash import argon2

DATABASE_URL = environ.get("DATABASE_URL", "postgres://localhost/rest_users_dev")

engine = create_engine(DATABASE_URL)

Session = sessionmaker(bind=engine)

@contextmanager
def db_session():
    s = Session()
    try:
        yield s
        s.commit()
    except:
        s.rollback()
        raise
    finally:
        s.close()

def select(s, q, **data):
    return [dict(r) for r in s.execute(text(q), data)]


Base = declarative_base(bind=engine)

class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    email = Column(String, nullable=False, unique=True)
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=False)
    password = Column(String, nullable=False)
    def set_password(self, clearpass):
        self.password = argon2.hash(clearpass)
    def has_password(self, clearpass):
        return argon2.verify(clearpass, self.password)
    @property
    def full_name(self):
        return "%s %s" % (self.first_name, self.last_name)
