from os import environ
from sqlalchemy import text
from email_validator import validate_email, EmailNotValidError
from flask import Flask, render_template, request, session, flash, url_for, redirect
from .models import db_session, User, select

app = application = Flask(__name__)
app.secret_key = environ["SECRET_KEY"]

@app.route("/users")
def get_users_index():
    with db_session() as s:
        users = s.query(User).all()
        return render_template("users_index.html", **locals())

@app.route("/users/new")
def get_users_new():
    return render_template("users_new.html", **locals())

@app.route("/users/create", methods=["POST"])
def post_users_new():
    fname = request.form.get("first_name", "")
    if len(fname) == 0:
        flash("Invalid First Name")
        return redirect(url_for("get_users_new"))
    lname = request.form.get("last_name", "")
    if len(lname) == 0:
        flash("Invalid Last Name")
        return redirect(url_for("get_users_new"))
    try:
        email = validate_email(request.form["email"])["email"]
    except (EmailNotValidError, KeyError):
        flash("Invalid Email")
        return redirect(url_for("get_users_new"))
    try:
        clearpass = request.form["password"]
        confirm = request.form["confirm"]
    except KeyError:
        flash("Invalid Password")
        return redirect(url_for("get_users_new"))
    if len(clearpass) == 0 or len(confirm) == 0 or clearpass != confirm:
        flash("Invalid Password")
        return redirect(url_for("get_users_new"))
    with db_session() as s:
        user = User()
        user.first_name = fname
        user.last_name = lname
        user.email = email
        user.set_password(clearpass)
        s.add(user)
        s.commit()
        uid = user.id
    return redirect(url_for("get_users_show", uid=uid))

@app.route("/users/<uid>/edit")
def get_users_edit(uid):
    with db_session() as s:
        users = s.query(User).filter(User.id == uid).all()
        return render_template("users_edit.html", **locals())

@app.route("/users/<uid>", methods=["POST"])
def post_users_edit(uid):
    fname = request.form.get("first_name", "")
    if len(fname) == 0:
        flash("Invalid First Name")
        return redirect(url_for("get_users_edit", uid=uid))
    lname = request.form.get("last_name", "")
    if len(lname) == 0:
        flash("Invalid Last Name")
        return redirect(url_for("get_users_edit", uid=uid))
    try:
        email = validate_email(request.form["email"])["email"]
    except (EmailNotValidError, KeyError):
        flash("Invalid Email")
        return redirect(url_for("get_users_new"))
    oldpass = request.form.get("old_password", "")
    clearpass = request.form.get("password", "")
    confirm = request.form.get("confirm", "")
    with db_session() as s:
        user = s.query(User).filter(User.id == uid).first()
        if user.has_password(oldpass):
            user.first_name = fname
            user.last_name = lname
            user.email = email
            if clearpass != "" and confirm != "" and clearpass == confirm:
                user.set_password(clearpass)
        uid = user.id
    return redirect(url_for("get_users_show", uid=uid))


@app.route("/users/<uid>")
def get_users_show(uid):
    with db_session() as s:
        users = s.query(User).filter(User.id==uid).all()
        return render_template("users_index.html", **locals())

@app.route("/users/<uid>/destroy", methods=["POST"])
def post_users_destroy(uid):
    with db_session() as s:
        s.query(User).filter(User.id==uid).delete()
        return redirect(url_for("get_users_index"))
